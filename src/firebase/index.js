import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'

const firebaseConfig = {
    apiKey: 'AIzaSyDH6ac4BsrAvUupqfww1BTxoZqQJELXRX4',
    authDomain: 'tbta-todo-3eef2.firebaseapp.com',
    projectId: 'tbta-todo-3eef2',
    storageBucket: 'tbta-todo-3eef2.appspot.com',
    messagingSenderId: '163357983337',
    appId: '1:163357983337:web:c2b153bb4262d4859918f8'
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)

// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(app);

export{
    db
}